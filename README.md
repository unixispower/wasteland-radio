# Wasteland Radio OS
Operating system setup script for the
[Wasteland Radio](https://blaines.world/projects/wasteland-radio.html).
This guide assumes the use of a Linux box for preparation and a Raspberry Pi
3B+ for the target SBC. Parts of this guide are taken from [the official Alpine
guide](https://wiki.alpinelinux.org/wiki/Raspberry_Pi). This install was last
tested on Alpine v3.14 for armv7 using a Raspberry Pi 3B+. 


## Preparation
The following steps are performed on a Linux host to prepare an SD card for the
Pi:

1. Download the latest [Alpine image](https://alpinelinux.org/downloads/) for
    Raspberry Pi (for 3B+ use the `armv7` image)
2. Insert the SD card into the computer
3. Create a bootable FAT32 partition on the card using
    [fdisk](https://wiki.alpinelinux.org/wiki/Create_a_Bootable_USB#Format_USB_stick)
    or a GUI tool like gparted
4. Mount the SD card
5. Extract the content of the downloaded Alpine image to the root of the card
6. Copy the file `usercfg.txt` and the directory `install` from this directory
    to the root of the SD card
7. Unmount and remove the SD card from the computer


## Installation
The following steps are performed on the Raspberry Pi:

1. Connect an ethernet cable -- a wired connection is required for install
2. Insert the SD card into the Pi and plug power in to turn it on
3. Login using username `root` and empty password
4. Run `cd /media/mmcbkl0p1/install; sh install` to run the installer
5. Run `reboot` to reboot into the initialized system


## Usage
Once the OS has been installed you can configure and boot it:

1. Copy the file `radio.conf` to a USB drive using a host computer
2. Insert the USB drive into an empty USB port on the Raspberry Pi
3. Power the Raspberry Pi on

The USB drive can be replugged after the Pi has booted. The radio player will
reload `radio.conf` when a USB device is automounted.


## Licensing
Source in this repository is licensed under the 2-clause BSD license, see
`LICENSE` for details.
